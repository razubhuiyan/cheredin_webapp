import uuid
from django.db import models
from django.urls import reverse

# Create your models here.

class Product(models.Model):
    
    id = models.UUIDField(

        primary_key = True,

        default = uuid.uuid4,

        editable = False

    )

    title = models.CharField(max_length=200)

    owner = models.CharField(max_length=200)

    description = models.TextField(max_length=500)

    contactInfo = models.TextField(max_length=300)



    def __str__(self):

        return self.title


    def get_absolute_url(self):
        return reverse("product_detail", args=[str(self.id)])
    
