from django.test import Client, TestCase
from django.urls import reverse


from .models import Product

# Create your tests here.


class ProductTests(TestCase):

    def setUp(self):

        self.product = Product.objects.create(

            title = 'Button mobile',

            owner = 'Shafi',

            description = 'Take it easy',

            contactInfo = 'xyz',

        )


    def test_product_listing(self):

        self.assertEqual(f'{self.product.title}', 'Button mobile')
        self.assertEqual(f'{self.product.owner}', 'Shafi')
        self.assertEqual(f'{self.product.description}', 'Take it easy')
        self.assertEqual(f'{self.product.contactInfo}', 'xyz')


    def test_product_list_view(self):

        response = self.client.get(reverse('product_list'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Button mobile')
        self.assertTemplateUsed(response, 'products/product_list.html')


    def test_product_detail_view(self):

        response = self.client.get(self.product.get_absolute_url())
        no_response = self.client.get('products/12345/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'Button mobile')
        self.assertTemplateUsed(response, 'products/product_detail.html')

